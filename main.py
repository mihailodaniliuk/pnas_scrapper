from bs4 import BeautifulSoup
import lxml
import undetected_chromedriver as uc
from selenium.webdriver.chrome.options import Options
from selenium.webdriver.common.by import By
from selenium.webdriver.support.wait import WebDriverWait
from selenium.webdriver.support import expected_conditions as EC
import os
import certifi

os.environ['SSL_CERT_FILE'] = certifi.where()


def open_webdriver():
    while True:
        try:
            global driver
            options = Options()
            options.add_argument("--no-sandbox")
            options.add_argument("--headless")
            options.add_argument('--start-maximized')
            options.add_argument("--disable-notifications")
            options.add_argument("--disable-gpu")
            options.add_argument("--disable-javascript")
            options.add_argument('--disable-dev-shm-usage')
            options.add_argument("--disable-renderer-backgrounding")
            options.add_argument("--disable-backgrounding-occluded-windows")
            options.add_argument("--disable-extensions")
            options.add_argument('--disable-application-cache')
            options.add_argument('--disable-browser-side-navigation')
            options.add_argument('--blink-settings=imagesEnabled=false')
            options.add_argument('--disk-cache-dir=/path/to/cache')
            driver = uc.Chrome(options=options, driver_executable_path='./chromedriver')
            driver.execute_cdp_cmd(
                "Browser.grantPermissions",
                {
                    "origin": 'https://www.google.com',
                    "permissions": ["geolocation"]
                },
            )
            driver.set_page_load_timeout(10)
            break
        except Exception as e:
            print(e)


def get_all_links_by_category(category_id):
    all_links = []
    for year in range(2012, 2013):
        driver.get(
            f'https://www.pnas.org/action/doSearch?field1=AllField&text1=&publication=&Ppub=&AfterMonth=1&AfterYear={year}&BeforeMonth=&BeforeYear={year}&access=on&sortBy=Earliest&pageSize=20&startPage=0&ConceptID={category_id}')
        main_page_soup = BeautifulSoup(driver.page_source, 'lxml')
        pagination_block = main_page_soup.find('ul', class_='pagination justify-content-center text-darker-gray')
        last_page = int(pagination_block.find_all('li')[-2].text)
        category_links = [
            f'https://www.pnas.org/action/doSearch?field1=AllField&text1=&publication=&Ppub=&AfterMonth=1&AfterYear={year}&BeforeMonth=&BeforeYear={year}&access=on&sortBy=Earliest&pageSize=20&startPage={page}&ConceptID={category_id}'
            for page in range(0, 1)]
        for category_link in category_links:
            driver.get(category_link)
            while True:
                try:
                    WebDriverWait(driver, 10).until(EC.presence_of_element_located((By.CSS_SELECTOR,
                                                                                    "#pb-page-content > div > div > main > section > div > div.container-fluid > div > div > div")))
                    break
                except Exception as e:
                    print(e)
                    pass
            soup = BeautifulSoup(driver.page_source, 'lxml')
            content_block = soup.find('div', class_='search-result__body items-results')
            hrefs = [f"{block.find('a').get('href')}" for block in content_block.find_all('div', class_='card')]
            all_links.extend([href for href in hrefs if href not in all_links])
        print(len(all_links))
    return all_links


def get_article_info(response_text):
    doi, title, date, abstract, significance = '', '', '', '', ''
    soup = BeautifulSoup(response_text, 'lxml')
    print(soup.prettify())
    return {'Doi': doi, 'Title': title, 'Date': date, 'Abstract': abstract, 'Siginificance': significance}


if __name__ == '__main__':
    open_webdriver()
    for category_id in [500084, 500088, 500380, 500083, 500090, 500080, 500065, 500092, 500097,
                        500094, 500073, 500086, 500076, 500060, 500066, 500278, 500376, 500062,
                        500093, 500070, 500378, 500091, 500095, 500075, 500082, 500096, 500079,
                        500072, 500067, 500061, 500379, 500089, 500074, 500077, 500071, 500098,
                        500078, 500377, 500279, 500381, 500286, 500375, 500068, 500069, 500081,
                        500284, 500087][:1]:
        article_links = get_all_links_by_category(category_id)
        for article_link in article_links[:1]:
            link = f'https://www.pnas.org{article_link}'
            print(link)
            driver.get(link)
            # print(driver.page_source)
            get_article_info(driver.page_source)
    driver.quit()
